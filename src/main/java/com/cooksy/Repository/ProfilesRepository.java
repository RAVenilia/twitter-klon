package com.cooksy.Repository;

import com.cooksy.Entity.Credentials;
import com.cooksy.Entity.Profiles;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by rose on 11/5/16.
 */
public interface ProfilesRepository extends JpaRepository<Profiles, Long> {
}
