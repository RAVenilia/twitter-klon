package com.cooksy.Repository;

import com.cooksy.Entity.Hash;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by 7 on 11/3/2016.
 */
public interface HashRepository extends JpaRepository<Hash, Long> {

    Hash findOneByLabel(String label);
    /*
        -> returns a list Hashes
     */
}
