package com.cooksy.Repository;

import com.cooksy.Entity.Credentials;
import com.cooksy.Entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by 7 on 11/3/2016.
 */
public interface UsersRepository extends JpaRepository<Users, Long> {

    List<Users> findAll();

    Users findByuserName(String username);

    Users findByCredential(Credentials credentials);

    Users findOneByUserName(String username);



    /*
        -> return a list of users

        -> return a user
     */
}
