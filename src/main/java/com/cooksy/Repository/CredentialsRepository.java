package com.cooksy.Repository;

import com.cooksy.Entity.Credentials;
import com.cooksy.Entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by rose on 11/5/16.
 */
public interface CredentialsRepository extends JpaRepository<Credentials, Long>{

    Credentials findByUserNameAndPassWord(String username, String password);

}
