package com.cooksy.Repository;

import com.cooksy.Entity.Tweets;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by 7 on 11/3/2016.
 */
public interface TweetsRepository extends JpaRepository<Tweets, Long> {
    Tweets findById(long id);

    List<Tweets> findAllByAuthor(String username);
    List<Tweets> findAllByInreplyof(long id);

    List<Tweets> findAllByRepostof(long id);
}
