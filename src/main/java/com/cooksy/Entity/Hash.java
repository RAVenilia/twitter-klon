package com.cooksy.Entity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by 7 on 11/3/2016.
 */

@Entity
@Table(name = "hash")
public class Hash {

    // Generating the table for the Hash Information
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "hashtag", updatable = false, nullable = false)
    private String label;

    // May need to implement the date else where
    @Column(name = "firstused", insertable = false)
    private Date firstUsed = new Date();


    @Column(name = "lastused", insertable = false)
    private Date lastUsed;


    @ManyToMany(mappedBy = "hash")
    private List<Tweets> tweets;

    public Hash(){

    }

    public Hash(long id, String label, Date firstUsed, Date lastUsed, List<Tweets> tweets) {
        this.id = id;
        this.label = label;
        this.firstUsed = firstUsed;
        this.lastUsed = lastUsed;
        this.tweets = tweets;
    }

    //---------------------------------------------------------------------------------------------------------------


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Date getFirstUsed() {
        return firstUsed;
    }

    public void setFirstUsed(Date firstUsed) {
        this.firstUsed = firstUsed;
    }

    public Date getLastUsed() {
        return lastUsed;
    }

    public void setLastUsed(Date lastUsed) {
        this.lastUsed = lastUsed;
    }

    public List<Tweets> getTweets() {
        return tweets;
    }

    public void setTweets(List<Tweets> tweets) {
        this.tweets = tweets;
    }
}
