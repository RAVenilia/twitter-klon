package com.cooksy.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;
import java.util.List;

/**
 * Created by 7 on 11/3/2016.
 */

@Entity
@Table(name = "users")
public class Users {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "username", unique = true, updatable = false)
    private String userName;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "joined", insertable = false, updatable = false)
    @JsonIgnore
    private Date joined = new Date();

    @Column(name = "flag", insertable = false)
    @JsonIgnore
    private Boolean flag = true;

    @Cascade(CascadeType.ALL)
    @OneToOne
    private Credentials credential;

    @Cascade(CascadeType.ALL)
    @OneToOne
    private Profiles profile;

    // I believe from the way i called upon my tweets from one user this may not be needed. Have not gotten to this point tho.
    @Cascade(CascadeType.ALL)
    @OneToMany
    private List<Tweets> tweetList;


    public Users(){

    }

    //---------------------------------------------------------------------------------------------------------------


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getJoined() {
        return joined;
    }

    public void setJoined(Date joined) {
        this.joined = joined;
    }

    public Boolean getFlag() {
        return flag;
    }

    public void setFlag(Boolean flag) {
        this.flag = flag;
    }

    public Credentials getCredential() {
        return credential;
    }

    public void setCredential(Credentials credential) {
        this.credential = credential;
    }

    public Profiles getProfile() {
        return profile;
    }

    public void setProfile(Profiles profile) {
        this.profile = profile;
    }

    public List<Tweets> getTweetList() {
        return tweetList;
    }

    public void setTweetList(List<Tweets> tweetList) {
        this.tweetList = tweetList;
    }

    @Override
    public String toString() {
        return "Users{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                ", joined=" + joined +
                ", flag=" + flag +
                ", credential=" + credential +
                ", profile=" + profile +
                ", tweetList=" + tweetList +
                '}';
    }
}
