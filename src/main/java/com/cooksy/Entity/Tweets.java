package com.cooksy.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.ManyToAny;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by 7 on 11/3/2016.
 */

@Entity
@Table(name = "tweets")
public class Tweets {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // Columns

    @Column(name = "author")
    private String author;


    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "timestamp", insertable = false, updatable = false)
    private Date created;

    @Column(name = "content", updatable = false, nullable = false)
    private String content;

    @OneToOne
    private Tweets inreplyof;

    @ManyToOne
    private Tweets repostof;

    @Column(name = "contentid")
    private int contentId;

    @Column(name = "flag")
    private boolean flag = true;

    @ManyToOne
    @JoinColumn
    @JsonIgnore
    private Users user;

    @ManyToMany
    @JoinTable(name = "HashTweets",
            joinColumns = @JoinColumn(name = "tweets_id"),
            inverseJoinColumns = @JoinColumn(name = "hash_id"))
    @JsonIgnore
    private List<Hash> hash;

    public Tweets(){

    }

    public Tweets(Long id, String author, Date created,
                  String content, Tweets inreplyof,
                  Tweets repostof, int contentId, boolean flag, Users user,
                  List<Hash> hash) {
        this.id = id;
        this.author = author;
        this.created = created;
        this.content = content;
        this.inreplyof = inreplyof;
        this.repostof = repostof;
        this.contentId = contentId;
        this.flag = flag;
        this.user = user;
        this.hash = hash;
    }

    //-----------------------------------------------------------------------------------------------------------------


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Tweets getInreplyof() {
        return inreplyof;
    }

    public void setInreplyof(Tweets inreplyof) {
        this.inreplyof = inreplyof;
    }

    public Tweets getRepostof() {
        return repostof;
    }

    public void setRepostof(Tweets repostof) {
        this.repostof = repostof;
    }

    public int getContentId() {
        return contentId;
    }

    public void setContentId(int contentId) {
        this.contentId = contentId;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public List<Hash> getHash() {
        return hash;
    }

    public void setHash(List<Hash> hash) {
        this.hash = hash;
    }
}
