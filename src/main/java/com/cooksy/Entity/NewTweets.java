package com.cooksy.Entity;

/**
 * Created by 7 on 11/6/2016.
 */
public class NewTweets {
    private String userName;
    private String passWord;
    private String content;

    public String getUserName() {
        return userName;
    }

    public NewTweets() {
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}


