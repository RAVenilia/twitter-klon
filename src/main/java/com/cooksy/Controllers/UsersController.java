package com.cooksy.Controllers;

import com.cooksy.Entity.Users;
import com.cooksy.Entity.Tweets;
import com.cooksy.Entity.Credentials;
import com.cooksy.Service.Users_Service;
import org.springframework.web.bind.annotation.*;


import java.util.List;

/**
 * Created by rose on 11/3/16.
 */

/*
NOTES ABOUT CONTROLLER AND UNFINISHED WORK CURRENTLY
------------------------------------------------------------------------------------------
    - Followings/Followers
    - Feeds
    - Delete -> This should be possible with modifying a flag that is attached to the user entity. Switching it to
        false and modifying methods to not look for false flags should handle that.
    - Alter Profile - > To approach this if Time allows I will create a seperate class that contains the information
        very much how i handled new Tweets. And then seperate it out so that the user can be updated. Do to time
        constraint. This will be looked at later.

    - Current Issues:
        When a user is created the Time stamp for join is not initiated.
 */


@RestController
@RequestMapping("users")
public class UsersController {
    //Call upon the User Service to be injected within the controller
    private Users_Service usersService;

    //Constructor
    public UsersController(Users_Service usersService) {
        this.usersService = usersService;
    }

    // A post command for creating a new user
    @PostMapping
    public Users activate(@RequestBody Users newUser){
        return usersService.activate(newUser);
        //return null;
    }

    //A Get Command for obtaining a list of users
    @GetMapping
    public List<Users> getAllUsers(){
        // This will be used to obtain a list of all current users
        System.out.println(usersService.getAll());
        return usersService.getAll();

    }

    // returns a user
    @GetMapping("/{username}")
    public Users getUser(@PathVariable String username){
        return usersService.getUser(username);
    }

    // This will allow for the Get command to obtain the list of tweets from a given user.
    @GetMapping("/{username}/tweets")
    public List<Tweets> getTweets(@PathVariable String username){
        return usersService.getTweets(username);
    }

    //----------------------------------------------------------------------------------------------------------------
    // From this point on is the creation of URL's to prepare for when implemented in the Service Class if Time allows


    // use the credentials and take in the username to be added to the database.
    @PostMapping("/{username}/follow")
    public String followUser(@PathVariable String username, @RequestBody Credentials credentials){
        //System.out.println("follow:"+username+"| credentials:"+credentials.getUserName());
        return usersService.followUser(credentials,username);
    }

    // Same as above except unfollowing
    @PostMapping("/{username}/unfollow")
    public String unfollowUser(@PathVariable String username, @RequestBody Credentials credentials){
        return usersService.unfollowUser(credentials,username);
    }

    // beginning of a feed list
    @GetMapping("/{username}/feed")
    public List<Tweets> getFeed(@PathVariable String username){
        return null;
    }

    //mentions
    @GetMapping("/{username}/mentions")
    public List<Tweets> getMentions(@PathVariable String username){
        return null;
    }
    //followers
    @GetMapping("/{username}/followers")
    public List<Users> getFollowers(@PathVariable String username){
        return null;
    }
    //following
    @GetMapping("/{username}/following")
    public List<Users> getFollowing(@PathVariable String username){
        return null;
    }

}
