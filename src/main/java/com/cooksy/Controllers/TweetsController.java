package com.cooksy.Controllers;

import com.cooksy.Entity.*;
import com.cooksy.Service.Hash_Service;
import com.cooksy.Service.Impl.Tweets_Impl;
import com.cooksy.Service.Tweets_Service;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by rose on 11/3/16.
 */
/*
NOTES ABOUT CONTROLLER AND UNFINISHED WORK CURRENTLY
-------------------------------------------------------------------------------------------------------------------
    - delete
        -> change the flag in the row to false and set it to be overlooked while being listed.
    - Like
    - reply
    - repost
    - mentions
        -> This can be handled easily in the service side in finding out who is being mentioned.
        This would require making a table to connect the user that is mentioned and Tweet associated with it.
    - CURRENT ISSUES
        -> Currently when a tweet is initialized the timestamp is not being set. More info on the Service Side
 */

// New set up for an URL
@RestController
@RequestMapping("tweets")
public class TweetsController {

    //Importing service,Hashservice was imported in case it is needed later. Most likely will be removed.
    Tweets_Service tweetsService;
    Hash_Service hashService;

    public TweetsController(Tweets_Service tweetsService, Hash_Service hashService) {
        this.tweetsService = tweetsService;
        this.hashService = hashService;
    }

    // In order to post a tweet i created a new Class called NewTweets, this took in the credential info and content
    // and then break it down to its components to make sure a proper user was creating a tweet.
    @PostMapping
    public Tweets newTweet(@RequestBody NewTweets newTweets) {
        return tweetsService.sendTweet(newTweets);
    }

    // Returns the Tweet at that Id.
    @GetMapping("/{id}")
    public Tweets getTweet(@PathVariable long id){
        return tweetsService.getTweet(id);
    }

    // Methods that have yet to be properly implemented
    //-----------------------------------------------------------------------------------------------------------------
    @DeleteMapping("/{id}")
    public Tweets deleteTweet(@PathVariable long id, @RequestBody Credentials creds){
        return null;
    }

    @PostMapping("/{id}/like")
    public String likeTweet(@PathVariable long id, @RequestBody Credentials creds){
        return "ERROR: Feature Not Implemented Yet";
    }

    @PostMapping("/{id}/reply")
    public Tweets replyTweet(@PathVariable long id, @RequestBody Credentials creds, @RequestBody String content){
        return tweetsService.replyToTweet(id,creds,content);
    }

    @PostMapping("/{id}/repost")
    public Tweets repostTweet(@PathVariable long id, @RequestBody Credentials creds){
        return null;
    }

    @GetMapping("/{id}/tags")
    public List<Hash> getHashTags(@PathVariable long id){
        return tweetsService.getHashes(id);
    }

    @GetMapping("/{id}/likes")
    public List<Users> getLikedUsers(@PathVariable long id){
        return null;
    }

    @GetMapping("/{id}/context")
    public Tweets_Impl.ContextObj getContext(@PathVariable long id){
        return tweetsService.getContext(id);
    }

    @GetMapping("/{id}/replies")
    public List<Tweets> getReplies(@PathVariable long id){
        return tweetsService.getReplies(id);
    }
    @GetMapping("/{id}/reposts")
    public List<Tweets> getReposts(@PathVariable long id){
        return tweetsService.getReposts(id);
    }
    @GetMapping("/{id}/mentions")
    public List<Tweets> getMentions(@PathVariable long id){
        //Ran out of time to get this set up
        return null;
    }
}


