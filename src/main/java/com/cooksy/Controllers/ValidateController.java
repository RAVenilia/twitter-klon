package com.cooksy.Controllers;

import com.cooksy.Service.Validate_Service;
import org.springframework.web.bind.annotation.*;

/**
 * Created by rose on 11/3/16.
 */
@RestController
@RequestMapping("validate")
public class ValidateController {

    private Validate_Service validateService;

    public ValidateController(Validate_Service validateService) {
        this.validateService = validateService;
    }

    @GetMapping("username/exist/{username}")
    public boolean userExist(@PathVariable String username) {
        //System.out.println("userExists:"+username);
        return validateService.userExist(username);
    }

    @GetMapping("tag/exist/{label}")
    public boolean tagExist(@PathVariable String label) {
        return validateService.tagExist(label);
    }

    @GetMapping("username/available/{username}")
    public boolean userAvail(@PathVariable String username) {
        return validateService.userAvailable(username);
    }
}
