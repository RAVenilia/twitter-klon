package com.cooksy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TwitterKlonApplication {

	public static void main(String[] args) {
		SpringApplication.run(TwitterKlonApplication.class, args);
	}
}
