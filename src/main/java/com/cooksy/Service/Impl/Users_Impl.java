package com.cooksy.Service.Impl;

import com.cooksy.Entity.Credentials;
import com.cooksy.Entity.Tweets;
import com.cooksy.Entity.Users;
import com.cooksy.Repository.CredentialsRepository;
import com.cooksy.Repository.ProfilesRepository;
import com.cooksy.Repository.TweetsRepository;
import com.cooksy.Repository.UsersRepository;
import com.cooksy.Service.Users_Service;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by rose on 11/3/16.
 */
/*
    NOTES ABOUT SERVICE AND UNFINISHED WORK
    --------------------------------------------------------------------------------------------------------
    This only contains completed or the beginnings of methods that will be implemented in the Controller.
    -> When creating a user Time stamp is not working. Need to look further into that.

    -> Followers and unfollowers has some work done on it. But remains incomplete.
 */

// Service for the Users
@Service
public class Users_Impl implements Users_Service {


    private UsersRepository usersRepository;
    private CredentialsRepository credentialsRepository;
    private ProfilesRepository profilesRepository;
    private TweetsRepository tweetsRepository;

    public Users_Impl(UsersRepository usersRepository, CredentialsRepository credentialsRepository, ProfilesRepository profilesRepository, TweetsRepository tweetsRepository) {
        this.usersRepository = usersRepository;
        this.credentialsRepository = credentialsRepository;
        this.profilesRepository = profilesRepository;
        this.tweetsRepository = tweetsRepository;
    }


    // Allows for the activation of a new user and then saves it
    @Override
    public Users activate(Users user) {
        return usersRepository.saveAndFlush(user);
    }

    // Returns a list of all users in the database
    @Override
    public List<Users> getAll() {
        return usersRepository.findAll();
    }

    // returns a users info
    @Override
    public Users getUser(String username) {
        return usersRepository.findByuserName(username);
    }

    // Returns a list of tweets by a user
    @Override
    public List<Tweets> getTweets(String username){
        return tweetsRepository.findAllByAuthor(username);
    }

    // This is the beginning of a followUser and unfollowUser methods
    public String followUser(Credentials creds, String username){
        if(!checkCreds(creds)){
            return "Error: Username & Password combo doesn't exist!";
        }
        Users follow=usersRepository.findByuserName(username);
        if(follow==null){
            return "Error: Username not found";
        }

        Users user = usersRepository.findByuserName(creds.getUserName());

        return null;
    }
    public String unfollowUser(Credentials creds, String username){
        if(!checkCreds(creds)){
            return "Error: Username & Password combo doesn't exist!";
        }
        Users follow=usersRepository.findByuserName(username);
        if(follow==null){
            return "Error: Username not found";
        }
        Users user = usersRepository.findByuserName(creds.getUserName());
        return null;
    }



    // This is a credential check which is used throughout the user service implementation.
    boolean checkCreds(Credentials cred){return credentialsRepository.findByUserNameAndPassWord(cred.getUserName(),cred.getPassWord())!=null;}

}
