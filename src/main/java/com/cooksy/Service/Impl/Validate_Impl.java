package com.cooksy.Service.Impl;

import com.cooksy.Repository.HashRepository;
import com.cooksy.Repository.UsersRepository;
import com.cooksy.Service.Validate_Service;
import org.springframework.stereotype.Service;

/**
 * Created by rose on 11/3/16.
 */
@Service
public class Validate_Impl implements Validate_Service{

    private UsersRepository usersRepository;
    private HashRepository hashRepository;

    public Validate_Impl(UsersRepository usersRepository,HashRepository hashRepository) {
        this.usersRepository = usersRepository;
        this.hashRepository=hashRepository;
    }

    public boolean userExist(String username){
       // System.out.println("userExist:"+username);
        return usersRepository.findOneByUserName(username)!=null;
    }

    public boolean userAvailable(String username){
        return !userExist(username);
    }

    public boolean tagExist(String label){
        return hashRepository.findOneByLabel(label)!=null;
    }


}
