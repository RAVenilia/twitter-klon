package com.cooksy.Service.Impl;

import com.cooksy.Entity.Credentials;
import com.cooksy.Entity.NewTweets;
import com.cooksy.Entity.Tweets;
import com.cooksy.Entity.Hash;
import com.cooksy.Entity.Users;
import com.cooksy.Repository.CredentialsRepository;
import com.cooksy.Repository.HashRepository;
import com.cooksy.Repository.TweetsRepository;
import com.cooksy.Repository.UsersRepository;
import com.cooksy.Service.Tweets_Service;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by rose on 11/3/16.
 */
/*
NOTES ABOUT SERVICE AND UNFINISHED WORK
-------------------------------------------------------------------------------------------------------------------------
    -> Handler Method
        -> This has not been fully created. Needs to implement a table that connects users and tweets. May need to use
        a new table to handle this. Due to the OneToMany relationship between users to tweets, ManyToOne between Tweets
        to User.


    -> Time Stamp problem. When seperating hashtags and checking for them. The time stamp will update after its been used.
    but it will not initialize once created.
 */

// Service for the tweet controllers.
@Service
public class Tweets_Impl implements Tweets_Service {
    private TweetsRepository tweetsRepository;
    private HashRepository hashRepository;
    private CredentialsRepository credentialsRepository;
    private UsersRepository usersRepository;

    public Tweets_Impl(TweetsRepository tweetsRepository, HashRepository hashRepository, CredentialsRepository credentialsRepository, UsersRepository usersRepository) {
        this.tweetsRepository = tweetsRepository;
        this.hashRepository = hashRepository;
        this.credentialsRepository = credentialsRepository;
        this.usersRepository = usersRepository;
    }

    // This the business logic behind the new Tweet being sent.
    /*
        This takes in the credential info from the New Tweet and then determines if there is a credential
        associated with that username and password. Once finished it then checks to see if it is null
        if not it will place the content into a new tweet then from there testing for hashing and possible mentions will be
        implemented.
     */
    public Tweets sendTweet(NewTweets newTweets) {
        Credentials cred = credentialsRepository.findByUserNameAndPassWord(newTweets.getUserName(), newTweets.getPassWord());
        if (cred != null) {
            Users user = usersRepository.findByCredential(cred);
            Tweets tweet = new Tweets();
            tweet.setAuthor(user.getUserName());
            tweet.setContent(newTweets.getContent());
            handleHashtags(tweet);
            //handleMentions(tweet);
            return tweetsRepository.saveAndFlush(tweet);
        } else {
            return null;
        }
    }

    // Setting up the beginnings of mentions
    void handleMentions(Tweets t){
        List<String> peeps = parseOutMentions(t.getContent());
        for(String u:peeps){
            Users user = usersRepository.findByuserName(u);
            //save the mention somehow. Probably have a List<Users> in the tweet for mentions. this would be a many-to-many with the Users
        }
    }

    // This was built to take in a tweet and then use a list of strings. The strings where tested to see if they
    // where part of the database already or not. Then it will add the new one to the database or update the last used
    // of the hashtag used.
     void handleHashtags(Tweets t){
        List<String> hashTags = parseOutHashtags(t.getContent());

        for(String ht : hashTags){
            Hash temp = hashRepository.findOneByLabel(ht);
            if(temp != null){
                System.out.println("Updating Hash");
                temp.setLastUsed(new Date());
            }else{
                System.out.println("Creating Hash");
                temp = new Hash();
                temp.setFirstUsed(new Date());
                temp.setLastUsed(new Date());
                temp.setLabel(ht);
            }
            hashRepository.save(temp);
            if(t.getHash()==null)
                t.setHash(new LinkedList<Hash>());
            t.getHash().add(temp);
        }
         hashRepository.flush();
    }



    //Almost Identical to the sendTweet above, except this adds the InReplyOf field.
    public Tweets replyToTweet(long replyId, Credentials cred, String content){
        Credentials creds = credentialsRepository.findByUserNameAndPassWord(cred.getUserName(), cred.getPassWord());
        Tweets r = tweetsRepository.findById(replyId);
        if (creds != null&&r!=null) {
            Users user = usersRepository.findByCredential(cred);
            Tweets tweet = new Tweets();
            tweet.setAuthor(user.getUserName());
            tweet.setContent(content);
            handleHashtags(tweet);
            tweet.setInreplyof(r);
            //handleMentions(tweet);
            return tweetsRepository.saveAndFlush(tweet);
        } else {
            return null;
        }
    }

    // Finding a tweet by the ID
    public Tweets getTweet(long id) {
        return tweetsRepository.findById(id);
    }

    public List<Tweets> getReplies(long id){
        return tweetsRepository.findAllByInreplyof(id);
    }

    public List<Tweets> getReposts(long id){
        return tweetsRepository.findAllByRepostof(id);
    }


    public List<Hash> getHashes(long id){

        //return hashRepository.findAllByTweetId(id);
        return null;
    }

    /*
        This is all theoretical and not sure if it would actually work, last minute why not try to psuedo code something
        Not gonna lie. THe 5 energy drinks promised me this would work.
     */

    //Tie the two list together creating mini class.
    public class ContextObj{

        public List<Tweets> before;
        public List<Tweets> after;
    }

    public ContextObj getContext(long id){
        ContextObj c = new ContextObj();
        Tweets t = tweetsRepository.findById(id);
        if(t != null){
            c.before  = getParentChain(t);
            c.after = getReplyChain(t);
            return c;
        }else
            return null;
    }

    public  List<Tweets> getParentChain(Tweets target){
        LinkedList<Tweets> chain= new LinkedList<Tweets>();
        return getParentChain(target.getInreplyof(),chain);
    }
    //recursion
     LinkedList<Tweets> getParentChain(Tweets target, LinkedList<Tweets> chain){
         //recursively go up the chain until we run out of tweets
        if(target == null)
            return chain;
        chain.add(target);

        return getParentChain(tweetsRepository.findOne(target.getInreplyof().getId()),chain);

    }

    public  LinkedList<Tweets> getReplyChain(Tweets target){
        LinkedList<Tweets> chain = new LinkedList<Tweets>();
        chain = getReplyChain(target,chain);
        //sort it in reverse order of dates
        //Using in line comparator to override the collections sort
        Collections.sort(chain,new Comparator<Tweets>(){
            public int compare(Tweets o1, Tweets o2){
                return o1.getCreated().compareTo(o2.getCreated());
            }
        });
        Collections.reverse(chain);
        return chain;
    }
    //grab all the chain and then dive down each children's children
     LinkedList<Tweets> getReplyChain(Tweets target,LinkedList<Tweets> chain){
        List<Tweets> children = tweetsRepository.findAllByInreplyof(target.getId());
        for(Tweets t :children){
            chain.add(t);
            getReplyChain(t,chain);
        }
        return chain;
    }
    /*
   Static helper methods:
       There are 3 methods here created to handle the Hashtags and the Mentions. In order to handle this one Method was used
       that took in different delimiters.

       The Main method that handles the parsing. A list of strings is created, The message is broken apart at the each
       delimiter. Then using a for loop we go through to each message and split at the " ". This allows us to otain the first
       element of the split. This is useful in case of a having other content past the Hasttag or mention.
    */
    public static List<String> parseOutHashtags(String message){

        return parseOutThings(message,"#");
    }
    public static List<String> parseOutMentions(String message){
        return parseOutThings(message,"@");
    }
    public static List<String> parseOutThings(String message, String delimiter){
        List<String> things = new LinkedList<String>();
        String[] working = message.split(delimiter);
        for(int i=1; i<working.length; i++){
            things.add(working[i].split(" ")[0]);
        }
        return things;
    }


}



