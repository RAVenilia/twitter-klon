package com.cooksy.Service;

import com.cooksy.Entity.Credentials;
import com.cooksy.Entity.Hash;
import com.cooksy.Entity.NewTweets;
import com.cooksy.Entity.Tweets;
import com.cooksy.Service.Impl.Tweets_Impl;

import java.util.List;

/**
 * Created by rose on 11/3/16.
 */
public interface Tweets_Service {


    Tweets sendTweet(NewTweets newTweets);

    Tweets getTweet(long id);

    List<Tweets> getReplies(long id);

    List<Tweets> getReposts(long id);

    Tweets_Impl.ContextObj getContext(long id);

    List<Hash> getHashes(long id);

    Tweets replyToTweet(long id, Credentials creds, String content);
}
