package com.cooksy.Service;

import com.cooksy.Entity.Credentials;
import com.cooksy.Entity.Tweets;
import com.cooksy.Entity.Users;

import java.util.List;

/**
 * Created by rose on 11/3/16.
 */
public interface Users_Service {
    Users activate(Users user);

    List<Users> getAll();

    Users getUser(String username);

    String followUser(Credentials creds, String username);
    String unfollowUser(Credentials creds, String username);

    List<Tweets> getTweets(String username);
}
