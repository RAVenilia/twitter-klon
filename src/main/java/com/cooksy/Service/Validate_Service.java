package com.cooksy.Service;

/**
 * Created by rose on 11/3/16.
 */
public interface Validate_Service {

    boolean userExist(String username);
    boolean tagExist(String label);
    boolean userAvailable(String username);
}
